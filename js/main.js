$(document).ready(function(){
        var aChildren = $("nav li").children(); // find the a children of the list items
        var aArray = []; // create the empty aArray
        for (var i=0; i < aChildren.length; i++) {    
            var aChild = aChildren[i];
            var ahref = $(aChild).attr('href');
            aArray.push(ahref);
        } // this for loop fills the aArray with attribute href values
        
        $(window).scroll(function(){
            var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
            var windowHeight = $(window).height(); // get the height of the window
            var docHeight = $(document).height();
            
            for (var i=0; i < aArray.length; i++) {
                var theID = aArray[i];
                var divPos = $(theID).offset().top; // get the offset of the div from the top of page
                var divHeight = $(theID).height(); // get the height of the div in question
                if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                    $("a[href='" + theID + "']").addClass("active");
                } else {
                    $("a[href='" + theID + "']").removeClass("active");
                }
            }
            
            if(windowPos + windowHeight == docHeight) {
                if (!$("nav li:last-child a").hasClass("active")) {
                    var navActiveCurrent = $(".active").attr("href");
                    $("a[href='" + navActiveCurrent + "']").removeClass("active");
                    $("nav li:last-child a").addClass("active");
                }
            }
        });
    });


//scroll

$(document).ready(function() {
  
  var scrollLink = $('.scroll');
  
  // Smooth scrolling
  scrollLink.click(function(e) {
    e.preventDefault();
    $('body,html').animate({
      scrollTop: $(this.hash).offset().top
    }, 1000 );
  });
  
  // Active link switching
  $(window).scroll(function() {
    var scrollbarLocation = $(this).scrollTop();
    
    scrollLink.each(function() {
      
      var sectionOffset = $(this.hash).offset().top - 20;
      
      if ( sectionOffset <= scrollbarLocation ) {
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
      }
    })
    
  })
  
})

//SCROLL CHANGES
$(document).scroll(function() {
  navbarScroll();
  hideCoffee();
});

function navbarScroll() {
  var y = window.scrollY;
  if (y > 500) {
    $('.navMain').addClass('navOpacity');
	
  } else if (y < 500) {
    $('.navMain').removeClass('navOpacity');
  }
}

//hide coffee
function hideCoffee() {
  var menu = document.getElementById("menu").clientHeight;
  var imgHeight = document.getElementById("imgHeight").clientHeight;
  var h = document.body.clientHeight -menu -imgHeight -1800;
  var y = window.scrollY;
 
  if (y > h) {
    $('.static').addClass('hideCoffee');
  } else if (y < h) {
    $('.static').removeClass('hideCoffee');
  }
}

//animation fade
new WOW().init();

//  dot nav
const dotNav = (elem, easing) => {
  function scrollIt(destination, duration = 200, easing = 'linear', callback) {
      const easings = {
          linear(t) { return t; },
          easeInQuad(t) { return t * t; },
          easeOutQuad(t) { return t * (2 - t); },
          easeInOutQuad(t) { return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t; },
          easeInCubic(t) { return t * t * t; },
          easeOutCubic(t) { return (--t) * t * t + 1; },
          easeInOutCubic(t) { return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1; },
          easeInQuart(t) { return t * t * t * t; },
          easeOutQuart(t) { return 1 - (--t) * t * t * t; },
          easeInOutQuart(t) { return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t; },
          easeInQuint(t) { return t * t * t * t * t; },
          easeOutQuint(t) { return 1 + (--t) * t * t * t * t; },
          easeInOutQuint(t) { return t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t; }
      };
      const start = window.pageYOffset;
      const startTime = 'now' in window.performance ? performance.now() : new Date().getTime();
      const documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
      const windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
      const destinationOffset = typeof destination === 'number' ? destination : destination.offsetTop;
      const destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset);
      if ('requestAnimationFrame' in window === false) {
          window.scroll(0, destinationOffsetToScroll);
          if (callback) {
              callback();
          }
          return;
      }
      function scroll() {
          const now = 'now' in window.performance ? performance.now() : new Date().getTime();
          const time = Math.min(1, ((now - startTime) / duration));
          const timeFunction = easings[easing](time);
          window.scroll(0, Math.ceil((timeFunction * (destinationOffsetToScroll - start)) + start));
          if (window.pageYOffset === destinationOffsetToScroll) {
              if (callback) {
                  callback();
              }
              return;
          }
          requestAnimationFrame(scroll);
      }
      scroll();
  }

  //  in viewport

  const inViewport = (el) => {
      let allElements = document.getElementsByTagName(el);
      let windowHeight = window.innerHeight;
      const elems = () => {
          for (let i = 0; i < allElements.length; i++) {  //  loop through the sections
              let viewportOffset = allElements[i].getBoundingClientRect();  //  returns the size of an element and its position relative to the viewport
              let top = viewportOffset.top;  //  get the offset top
              if(top < windowHeight){  //  if the top offset is less than the window height
                  allElements[i].classList.add('in-viewport');  //  add the class
              } else{
                  allElements[i].classList.remove('in-viewport');  //  remove the class
              }
          }
      }
      elems();
      window.addEventListener('scroll', elems);
  }
  inViewport('section');

  //  dot nav

  const allSecs = document.getElementsByTagName(elem);
  const nav = document.getElementById('dot-nav');
  const scrollSpeed = '1000';
  let allVis = document.getElementsByClassName('in-viewport'),
      allDots;
  for (let i = 0; i < allSecs.length; i++) {
      allSecs[i].classList.add('section-' + i);
  }

  //  add the dots

  const buildNav = () => {
      for (let i = 0; i < allSecs.length; i++) {
          const dotCreate = document.createElement('a');
          dotCreate.id = 'dot-' + i;
          dotCreate.classList.add('dots');
          dotCreate.href = '#';
          dotCreate.setAttribute('data-sec', i);
          nav.appendChild(dotCreate);
      }
  }
  buildNav();

  //  nav position

  let navHeight = document.getElementById('dot-nav').clientHeight;
  let hNavHeight = navHeight / 2;
  document.getElementById('dot-nav').style.top = 'calc(50% - ' + hNavHeight + 'px)';

  //  onscroll

  const dotActive = () => {
      allVis = document.getElementsByClassName('in-viewport');
      allDots = document.getElementsByClassName('dots');
      visNum = allVis.length;
      let a = visNum - 1;
      for (let i = 0; i < allSecs.length; i++) {
          allDots[i].classList.remove('active');
      }
      document.getElementById('dot-' + a).classList.add('active');
  }
  dotActive();
  window.onscroll = function(){ dotActive(); };

  //  click stuff

  const scrollMe = (e) => {
      let anchor = e.currentTarget.dataset.sec;
      scrollIt(document.querySelector('.section-' + anchor), scrollSpeed, easing);
      e.preventDefault();
  }

  allDots = document.getElementsByClassName('dots');
  for (let i = 0; i < allDots.length; i++) {
      allDots[i].addEventListener('click', scrollMe);
  }

}

dotNav('section', 'easeInOutCubic');

//file input

function bs_input_file() {
	$(".input-file").before(
		function() {
			if ( ! $(this).prev().hasClass('input-ghost') ) {
				var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
				element.attr("name",$(this).attr("name"));
				element.change(function(){
					element.next(element).find('input').val((element.val()).split('\\').pop());
				});
				$(this).find("button.btn-choose").click(function(){
					element.click();
				});
				$(this).find("button.btn-reset").click(function(){
					element.val(null);
					$(this).parents(".input-file").find('input').val('');
				});
				$(this).find('input').css("cursor","pointer");
				$(this).find('input').mousedown(function() {
					$(this).parents('.input-file').prev().click();
					return false;
				});
				return element;
			}
		}
	);
}
$(function() {
	bs_input_file();
});