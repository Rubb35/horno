<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Horno Urbano | pan café & más</title>
	<link href="css/bootstrap-4.0.0.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<!--ICO-->
	<link rel="shortcut icon" href="images/ico/favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	
  </head>
  <body>
  	<section id="thanks">
		<div class="container">
			<div class="row">
			<div class="col-lg-4 text-center m-auto">
		    	<img class="img-fluid LogoThanks" src="images/logo.png" alt="Horno Urbano | pan café & más"/> 
				<hr>
				<h1></h1>
				<h3>Gracias por escribirnos</h3>
				<p>Su mensaje se ha enviado con éxito</p>
			<button type="button" class="btn btn-outline-dark mt-3" onclick="location.href='http://hornourbano.mx/index.html#menu';">Regresar al sitio</button>
			</div>
		</div>
		</div>
	</section>


	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap-4.0.0.js"></script>
  </body>
</html> 
 
 <?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
 
if($_POST && isset($_FILES['file']))
{
    $recipient_email    = "contacto@hornourbano.mx"; //recepient
    $from_email         = "contactoweb@hornourbano.mx"; //from email using site domain.
    $subject            = "Mensaje de hornourbano.mx"; //email subject line
   
    $sender_name = filter_var($_POST["s_name"], FILTER_SANITIZE_STRING); //capture sender name
    $sender_email = filter_var($_POST["s_email"], FILTER_SANITIZE_STRING); //capture sender email
    $sender_message = filter_var($_POST["s_message"], FILTER_SANITIZE_STRING); //capture message
	$sender_tel = filter_var($_POST["s_tel"], FILTER_SANITIZE_STRING); //capture message
    $attachments = $_FILES['file'];
   
    //php validation
    if(strlen($sender_name)<4){
        die('Por favor, escribe tu nombre completo');
    }
    if (!filter_var($sender_email, FILTER_VALIDATE_EMAIL)) {
      die('El correo no tiene el formato esperado');
    }
	 if(strlen($sender_tel)<4){
        $sender_tel = "Sin dato";
    }
    if(strlen($sender_message)<4){
        die('Por favor, escribe un mensaje con más detalle');
    }
   
    $file_count = count($attachments['name']); //count total files attached
    $boundary = md5("hornourbano.mx");
           
    if($file_count > 0){ //if attachment exists
        //header
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "From:".$from_email."\r\n";
        $headers .= "Reply-To: ".$sender_email."" . "\r\n";
        $headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";
       
        //message text
        $body = "--$boundary\r\n";
        $body .= "Content-Type: text/plain; charset=UTF-8\r\n";
        $body .= "Content-Transfer-Encoding: base64\r\n\r\n";
		$body .= chunk_split(base64_encode("Mensaje enviado por:  " . $sender_name . "\r\n\r\nCorreo de contacto: " . $sender_email . "\r\n\r\nTeléfono de contacto: " . $sender_tel ."\r\n\r\nMensaje: " .$sender_message));
       // $body .= "Mensaje enviado por: <b> " . $sender_name . "</b> </br> Correo de contacto: <b><a href='mailto:". $sender_email."'>" . $sender_email . "</a></b></br> " .$sender_message;

        //attachments
        for ($x = 0; $x < $file_count; $x++){      
            if(!empty($attachments['name'][$x])){
               
                if($attachments['error'][$x]>0) //exit script and output error if we encounter any
                {
                    $mymsg = array(
                    1=>"El archivo excede el tamaño permitido.",
                    2=>"El archivo excede el tamaño permitido.",
                    3=>"El archivo fue cargado parcialmente",
                    4=>"No se cargo el archivo",
                    6=>"No se encuentra el folder temporal" );
                    die($mymsg[$attachments['error'][$x]]);
                }
               
                //get file info
                $file_name = $attachments['name'][$x];
                $file_size = $attachments['size'][$x];
                $file_type = $attachments['type'][$x];
               
                //read file
                $handle = fopen($attachments['tmp_name'][$x], "r");
                $content = fread($handle, $file_size);
                fclose($handle);
                $encoded_content = chunk_split(base64_encode($content)); //split into smaller chunks (RFC 2045)
               
                $body .= "--$boundary\r\n";
                $body .="Content-Type: $file_type; name=".$file_name."\r\n";
                $body .="Content-Disposition: attachment; filename=".$file_name."\r\n";
                $body .="Content-Transfer-Encoding: base64\r\n";
                $body .="X-Attachment-Id: ".rand(1000,99999)."\r\n\r\n";
                $body .= $encoded_content;
            }
        }

    }else{ //send plain email otherwise
       $headers = "From:".$from_email."\r\n".
        "Reply-To: ".$sender_email. "\n" .
        "X-Mailer: PHP/" . phpversion();
        $body = $sender_message;
    }
       
     $sentMail = @mail($recipient_email, $subject, $body, $headers);
    if($sentMail) //output success or failure messages
    {      
		header( "refresh:5; url=index.html#menu" );
        die('');
    }else{
        die('El mensaje no pudo ser enviado, revisa tus datos.');  
    }
}
?>
