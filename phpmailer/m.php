<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$para = "contacto@hornourbano.mx"; 
$asunto = "Mensaje de prueba"; 
$mensaje = "Esto es un <b>mesaje de prueba, envío desde el sitio </b>";
$mensaje_nohtml = "Esto es un mesaje de prueba. Para correos que no soporten HTML";

require 'PHPMailerAutoload.php';
$mail = new PHPMailer(true); 
$mail->isSMTP();
$mail->Host = 'relay-hosting.secureserver.net';
$mail->Port = 25;
$mail->SMTPAuth = false;
$mail->SMTPSecure = false;

//Envío
$mail->SetFrom('contacto@hornourbano.mx', 'Contacto Web');
$mail->addAddress($para, 'Contacto');

//Mensaje
$mail->WordWrap = 50; // Máxima longitud de las líneas
$mail->IsHTML(true);
$mail->Subject = $asunto;
$mail->Body = $mensaje;
$mail->AltBody = $mensaje_nohtml;

//Comprobaciones
if(!$mail->send()) {
echo 'El mensaje no se ha podido enviar.';
echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
echo 'El mensaje se ha enviado a ' . $para;
}
?>