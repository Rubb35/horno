<?php

define( 'GD_VIP', '104.238.71.97' );
define( 'GD_RESELLER', 1 );
define( 'GD_ASAP_KEY', '1e74daaa7283566c790c777e6b7d074d' );
define( 'GD_STAGING_SITE', false );
define( 'GD_EASY_MODE', true );
define( 'GD_SITE_CREATED', 1553886940 );
define( 'GD_ACCOUNT_UID', '6b1bbf27-5254-11e9-8166-3417ebe72601' );
define( 'GD_SITE_TOKEN', '48fa0e48-3ec6-45ab-90ee-1e1faddfdc70' );
define( 'GD_TEMP_DOMAIN', 'yhp.edf.myftpupload.com' );
define( 'GD_CDN_ENABLED', FALSE );
define( 'GD_GF_LICENSE_KEY', 'YBmsydb9wSNiyVpyyztH9G9AWNYIyfWv' );

// Newrelic tracking
if ( function_exists( 'newrelic_set_appname' ) ) {
	newrelic_set_appname( '6b1bbf27-5254-11e9-8166-3417ebe72601;' . ini_get( 'newrelic.appname' ) );
}

/**
 * Is this is a mobile client?  Can be used by batcache.
 * @return array
 */
function is_mobile_user_agent() {
	return array(
	       "mobile_browser"             => !in_array( $_SERVER['HTTP_X_UA_DEVICE'], array( 'bot', 'pc' ) ),
	       "mobile_browser_tablet"      => false !== strpos( $_SERVER['HTTP_X_UA_DEVICE'], 'tablet-' ),
	       "mobile_browser_smartphones" => in_array( $_SERVER['HTTP_X_UA_DEVICE'], array( 'mobile-iphone', 'mobile-smartphone', 'mobile-firefoxos', 'mobile-generic' ) ),
	       "mobile_browser_android"     => false !== strpos( $_SERVER['HTTP_X_UA_DEVICE'], 'android' )
	);
}